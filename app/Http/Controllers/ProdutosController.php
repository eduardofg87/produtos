<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Produto;
use Session;
use Illuminate\Support\Facades\Auth;
use Log;

class ProdutosController extends Controller
{
    public function index(){
        $produtos = Produto::paginate(4);
        return view('produto.index', array('produtos' =>$produtos,'busca'=>null));
    }
    public function show($id){
        $produto = Produto::find($id);
        return view('produto.show', array('produto' =>$produto));
    }
    public function create(){
        if(Auth::check()){
            return view('produto.create');
        }
        else {
            return redirect('login');
        }
    }
    public function store(Request $request){
        if(Auth::check()){
            $this->validate($request,[
                'referencia' => 'required|unique:produtos|min:3',
                'titulo' => 'required|min:3',
            ]);
            $produto = new Produto();
            $produto->referencia = $request->input('referencia');
            $produto->titulo = $request->input('titulo');
            $produto->descricao = $request->input('descricao');
            $produto->preco = $request->input('preco');

            if($produto->save()){
                return redirect('produtos');
            }
        }
        else {
            return redirect('login');
        }
    }
    public function edit($id){
        if(Auth::check()){
            $produto = Produto::find($id);
            return view('produto.edit', array('produto'=>$produto));
        }
        else {
            return redirect('login');
        }
    }
    public function update($id, Request $request){
        if(Auth::check()){
            $produto = Produto::find($id);
            $this->validate($request,[
                'referencia' => 'required|min:3',
                'titulo' => 'required|min:3',
            ]);
            if($request->hasFile('fotoproduto')){
                $imagem = $request->file('fotoproduto');
                $nomearquivo = md5($id) .".".$imagem->getClientOriginalExtension();
                $request->file('fotoproduto')->move(public_path('./img/produtos/'), $nomearquivo);
            }
            $produto->referencia = $request->input('referencia');
            $produto->titulo = $request->input('titulo');
            $produto->descricao = $request->input('descricao');
            $produto->preco = $request->input('preco');
            $produto->save();
            Session::flash('mensagem', 'Produto alterado com sucesso.');
            //activity()->log('Look mum, I logged something');
            return redirect()->back();
        }
        else {
            return redirect('login');
        }
    }
    public function destroy($id){
        
        if(Auth::check()){
            $produto = Produto::find($id);
            $produto->delete();
            Session::flash('mensagem', 'Produto excluído com sucesso.');
            return redirect()->back();
        }
        else {
            return redirect('login');
        }
    }
    public function buscar(Request $request){
        $produtos = Produto::where('titulo','LIKE','%'.$request->input('busca').'%')->orwhere('descricao','LIKE','%'.$request->input('busca').'%')->paginate(4);
        return view('produto.index', array('produtos'=>$produtos,'busca'=>$request->input('busca')));
    }
    public function extras(){
        $produtos = Produto::orderBy('id','DESC')->get();
        foreach($produtos as $produto){
            echo "<h2>" . $produto->id . " - " . $produto->titulo . "</h2>";
        }
        $menor_preco = Produto::all()->min('preco');
        $media_preco = Produto::all()->avg('preco');
        $maior_preco = Produto::all()->max('preco');
        $contar = Produto::all()->count();
        $soma = Produto::all()->sum('preco');
        echo "Produto mais barato: R$".number_format($menor_preco,2,",","."). "<br/>";
        echo "Media de preço: R$".number_format($media_preco,2,",","."). "<br/>";
        echo "Produto mais caro: R$".number_format($maior_preco,2,",","."). "<br/>";
        echo "Para ".$contar. " produtos <br/>";
        echo "Soma dos preços: R$".number_format($soma,2,",","."). "<br/>";

    }
    public function xml(){
        $meus_links = array();
 
        $meus_links[0]['id'] = '1';
        $meus_links[0]['title'] = 'Teste 1';
        $meus_links[0]['description'] = 'Desc 1';
        $meus_links[0]['image'] = 'Image 1';
        
        $meus_links[1]['id'] = '2';
        $meus_links[1]['title'] = 'Teste 2';
        $meus_links[1]['description'] = 'Desc 2';
        $meus_links[1]['image'] = 'Image 2';
        
        $meus_links[2]['id'] = '3';
        $meus_links[2]['title'] = 'Teste 3';
        $meus_links[2]['description'] = 'Desc 3';
        $meus_links[2]['image'] = 'Image 3';
        // Receberá todos os dados do XML
        $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>'.PHP_EOL;
        
        // A raiz do meu documento XML
        $xml .= '<links>'.PHP_EOL;
        
        // Loop dos valores
        for ( $i = 0; $i < count( $meus_links ); $i++ ) {
            $xml .= '<link>'.PHP_EOL;
            $xml .= '<id>' . $meus_links[$i]['id'] . '</id>'.PHP_EOL;
            $xml .= '<title>' . $meus_links[$i]['title'] . '</title>'.PHP_EOL;
            $xml .= '<description>' . $meus_links[$i]['description'] . '</description>'.PHP_EOL;
            $xml .= '<image>' . $meus_links[$i]['image'] . '</image>'.PHP_EOL;
            $xml .= '</link>'.PHP_EOL;
        }
        
        // Fechamento da raiz
        $xml .= '</links>';
        
        // Escreve o arquivo
        //$xml = preg_replace('/([\x00-\x08]|[\x0B-\x0C]|[\x0E-\x1F]|[\x7F-\x9F])/', '', $xml);
        
        //dd($xml);

        $fp = fopen('meus_links.xml', 'w+');
        fwrite($fp, $xml);
        fclose($fp);
        echo "Arquivo xml criado com sucesso!<br/>";
        // Faz o load do arquivo XML e retorna um objeto
        $arquivo_xml = simplexml_load_file('meus_links.xml');
        echo '<hr>';
        // Loop para ler o objeto
        //dd($arquivo_xml);
        for ( $j = 0; $j < count( $arquivo_xml ); $j++ ) {
            // Imprime o valor o valor da tag <id></id>
            echo $arquivo_xml->link[$j]->id . '<br>';
            
            // Imprime o valor o valor da tag <title></title>
            echo $arquivo_xml->link[$j]->title . '<br>';
            
            // Imprime o valor o valor da description <description></description>
            echo $arquivo_xml->link[$j]->description . '<br>';
            
            // Imprime o valor o valor da description <image></image>
            echo $arquivo_xml->link[$j]->image . '<br>';
            
            // Apenas uma quebra de linha a mais
            echo '<hr>';
        }
        
    }
    public function parsexml(){
        $arquivo_xml = simplexml_load_file('B0010703.181.xml');
        //$arquivo_xml = simplexml_load_file('meus_links.xml');
        dd($arquivo_xml);
        for ( $j = 0; $j < count( $arquivo_xml ); $j++ ) {
            echo $arquivo_xml->remessa[$j]->hd['01'] . '<br>';
            //echo $arquivo_xml->remessa[$j]->tr . '<br>';
            //echo $arquivo_xml->remessa[$j]->tl . '<br>';
        }
        
    }
    public function file(){
        Log::info('teste log');
        //$fp = fopen("./dados.txt","r");
        //fwrite($fp,"Hello world!");
        //$text = fgets($fp,100);
        //dd($text);
        //while (false !== ($char = fgetc($fp))) {
        //    echo "$char\n";
        //}
        //fclose($fp);
        
        //$file_lines = file("./B0010703.181");
        //$file_lines = fopen("./B0010703.181","r");
        
        //CONFIRMAÇÃO
        $file_lines = fopen("./R0012606.181","r");
        //RETORNO
        //$file_lines = fopen("./R2370803.181","r");
        //echo "qtd linhas: " . count($file_lines)."<br/>";
        while (false !== ($text = fgets($file_lines,1000))) {
            echo $text."<br/>";
            switch($text[0]){
                case "0":
                    $header[0] = substr($text,0,1);
                    $header[1] = substr($text,1,3);
                    $header[2] = substr($text,4,40);
                    $header[3] = substr($text,44,8);
                    $header[4] = substr($text,52,3);
                    $header[5] = substr($text,55,3);
                    $header[6] = substr($text,58,3);
                    $header[7] = substr($text,61,6);
                    $header[8] = substr($text,67,4);
                    $header[9] = substr($text,71,4);
                    $header[10] = substr($text,75,4);
                    $header[11] = substr($text,79,4);
                    $header[12] = substr($text,83,6);
                    $header[13] = substr($text,89,3);
                    $header[14] = substr($text,92,7);
                    $header[15] = substr($text,99,497);
                    $header[16] = substr($text,596,4);
                    for ($j=0; $j<17; $j++){
                        echo "header".($j+1).": ".$header[$j]."<br/>";
                    }
                    break;
                case "1":
                    $transacao[0] = substr($text,0,1);
                    $transacao[1] = substr($text,1,3);
                    $transacao[2] = substr($text,4,15);
                    $transacao[3] = substr($text,19,45);
                    $transacao[4] = substr($text,64,45);
                    $transacao[5] = substr($text,109,14);
                    $transacao[6] = substr($text,123,45);
                    $transacao[7] = substr($text,168,8);
                    $transacao[8] = substr($text,176,20);
                    $transacao[9] = substr($text,196,2);
                    $transacao[10] = substr($text,198,15);
                    $transacao[11] = substr($text,213,3);
                    $transacao[12] = substr($text,216,11);
                    $transacao[13] = substr($text,227,8);
                    $transacao[14] = substr($text,235,8);
                    $transacao[15] = substr($text,243,3);
                    $transacao[16] = substr($text,246,14);
                    $transacao[17] = substr($text,260,14);
                    $transacao[18] = substr($text,274,20);
                    $transacao[19] = substr($text,294,1);
                    $transacao[20] = substr($text,295,1);
                    $transacao[21] = substr($text,296,1);
                    $transacao[22] = substr($text,297,45);
                    $transacao[23] = substr($text,342,3);
                    $transacao[24] = substr($text,345,14);
                    $transacao[25] = substr($text,359,11);
                    $transacao[26] = substr($text,370,45);
                    $transacao[27] = substr($text,415,8);
                    $transacao[28] = substr($text,423,20);
                    $transacao[29] = substr($text,443,2);
                    $transacao[30] = substr($text,445,2);
                    $transacao[31] = substr($text,447,10);
                    $transacao[32] = substr($text,457,1);
                    $transacao[33] = substr($text,458,8);
                    $transacao[34] = substr($text,466,10);
                    $transacao[35] = substr($text,476,1);
                    $transacao[36] = substr($text,477,8);
                    $transacao[37] = substr($text,485,2);
                    $transacao[38] = substr($text,487,20);
                    $transacao[39] = substr($text,507,10);
                    $transacao[40] = substr($text,517,6);
                    $transacao[41] = substr($text,523,10);
                    $transacao[42] = substr($text,533,5);
                    $transacao[43] = substr($text,538,15);
                    $transacao[44] = substr($text,553,3);
                    $transacao[45] = substr($text,556,1);
                    $transacao[46] = substr($text,557,8);
                    $transacao[47] = substr($text,565,1);
                    $transacao[48] = substr($text,566,1);
                    $transacao[49] = substr($text,567,10);
                    $transacao[50] = substr($text,577,19);
                    $transacao[51] = substr($text,596,4);
                    for ($j=0; $j<52; $j++){
                        echo "transação".($j+1).": ".$transacao[$j]."<br/>";
                    }
                    break;
                case "9":
                    $trailler[0] = substr($text,0,1);
                    $trailler[1] = substr($text,1,3);
                    $trailler[2] = substr($text,4,40);
                    $trailler[3] = substr($text,44,8);
                    $trailler[4] = substr($text,52,5);
                    $trailler[5] = substr($text,57,18);
                    $trailler[6] = substr($text,75,521);
                    $trailler[7] = substr($text,596,4);
                    for ($j=0; $j<8; $j++){
                        echo "trailler".($j+1).": ".$trailler[$j]."<br/>";
                    }
                    break;
            }
        }
        fclose($file_lines);
        //$file_lines[0] = "abc";
        //$file_lines[1] = "def";
        //$arq = array();
        //array_push($arq, "abc".PHP_EOL);
        //array_push($arq, "def".PHP_EOL);
        //array_push($arq, "ghi".PHP_EOL);
        //file_put_contents("./dados.txt",$arq);
        
    }
}
