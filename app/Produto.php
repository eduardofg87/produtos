<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
class Produto extends Model
{
    use LogsActivity;

    protected static $logFillable = true;

    public $table = 'produtos';
    protected $fillable = [
        'id',
		'referencia',
		'titulo',
		'descricao',
		'preco'
    ];

}
